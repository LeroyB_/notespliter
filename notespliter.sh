#!/bin/bash
# [DEBUG] set -x for a advanced logging
#   set -x
#
#   #################################################################################
#   #                                                                               #
#   #                                                                               #
#   #                                                                               #
#   #                                                                               #
#   #                               READ THIS FIRST                                 #
#   #                                                                               #
#   #                                                                               #                         
#   #                          Start with the WHAT section,                         #
#   #               that is located at the bottem left of the textbox.              #
#   #                                                                               #
#   #                                                                               #
#   #                                                                               #
#   #                                                                               #
#   #################################################################################
#
# WHAT ========================================================================
#
#   1)  This text file called "notespliter.sh" is a script. It is written in the language "BASH".
#       NoteSpliter takes an input file, encrypts it with a user choosen password and 
#       breaks it apart into a number of pieces with the amount being choosen by the user.
#
#   2)  NoteSpliter can also take a directory of NoteSpliter pieces and concatenate, decode then
#       decrypt them to recreate the original file.
#       The pieces have the file extetion ".leroy". Are all of them needed?
#
#   3)  Lines in this script starting with a symbol like this "#" are comments and
#       are therefor not relevant for the script. They are used to explain the code.
#       
#   4)  Look for the numbers on the right of the page. 
#       They point towards point of interest, within the code
#       in ascending order. If you follow them in order, you'll follow the execution path of
#       the script. They look like this below. Search for comments like this on the right 
#       side of the screen to read more about how this script workes.
#
#                                                                                                     #
#                                                                                                     # 2.1 [START] 
#                                                                                                     # This is a description.
#                                                                                                     #
#
#
#                                                                                                     #
#                                                                                                     # 2.1 [END] 
#                                                                                                     #
#
#
# GOAL =======================================================================
#
#   This project is about writing a script and annotate said script with explanations and
#   general information. The script "NoteSpliter" can take an input such as a 
#   [ textfile.txt || archive.zip || image.png ], 
#   encrypts, hex encodes and then breaks it apart into n equal pieces.
#
#   These pieces can then be stored at diffrent locations. They can also be put back together. Optionally not all pieces have to be returned.
#
# VERSION 1.1 =================================================================
#
# TODO ========================================================================
#   -[Test] Image as input.
#  ->  Workes great :)
#   -[Test] What if I don't have all the pieces? maybe only the last one is missing.
#
#   -[BUG] Can't split into n pieces. Right now it just does 7 pieces. Spliting by size workes.
#     needs more testing. I'll leave my testigs in for now.
#  -> Seems like this is fixed
#
#   -[ADDON] Option to chech if the original file should be deleted opon spliting.
#   -[ADDON] Format code and comments.
#   -[ADDON] Write more specific and ascending [LOG] statmens.
#
# USAGE =======================================================================
#
#   Download this file by running the following commands in the cli, save the file and
#   make it executable.
#
#   $ wget https://gitlab.com/bonventre_ch/notespliter/-/raw/master/notespliter.sh
#   $ chmod +x notespliter.sh
#   $ ./notespliter split -i file -n name -p pass -s n OR -b n[k|m]
#   where -i = input file/archive
#         -n = a unique name identifying this call
#         -p = the passphrase used to encrypt the input
#         -s = split the input into n pieces of equal bytes, OR
#         -b = split the input into x pieces of max n[k|m] bytes
#              k represents kilobytes, m megabytes; example: -b 1k, -b 20m
#              if no unit is provided bytes are assumed; example: -b 500
#
#   ↳
#   Creates a new notespliter inside a folder (named by -n).  Note that only the -s
#   or -b flag need be specified at a time, never both.
#
#
#   $ ./notespliter join -i folder/of/pieces -p pass
#   where -i = a folder containing notespliter pieces
#         -p = the password used to decrypt this notespliter
#
#   ↳
#   Concatenates, decodes and decrypts a notespliter to recreate the original file.
#

# CREDIT for the idea and the joining script: https://github.com/kndyry
#   Forked: 06.07.12020
#   Updated: 19.08.12021
#   Made by Leroy



#                                                                                                     #
#                                                                                                     # 3 [Start] 
#                                                                                                     # This is global scope. The script starts by asigning
#                                                                                                     # global variables and defining a few functions.
#                                                                                                     #



#   When "errtrace" is enabled, the ERR trap is also triggered when the error 
#   (a command returning a nonzero code) occurs inside a function or a subshell. 
#   Another way to put it is that the context of a function or 
#   a subshell does not inherit the ERR trap unless errtrace is enabled.

#   "pipefail" causes a pipeline (for example, curl -s https://sipb.mit.edu/ | grep foo) 
#   to produce a failure return code if any command errors. 
#   Normally, pipelines only return a failure if the last command errors.
#   In combination with set -e (or maybe: "set -o errtrace"),
#   this will make the script exit if any command in a pipeline errors.
set -o errtrace
set -o pipefail

gpg=$(command -v gpg || command -v gpg2)

#   just for clean logs with one parmeter from the call
func_failMsgWithLog () {

#   set the terminal output to red; 0 = black, 1 = red, 2 = green, 3 = yellow, 4 = blue 

#   SELECT GRAPHIC RENDITION and the 0 means = “default rendition (implementation-defined), 
#   cancels the effect of any preceding occurrence of SGR in the data stream regardless of 
#   the setting of the GRAPHIC RENDITION COMBINATION MODE (GRCM)”

# print the message to the cli and exit with code "exit 1" to indicate an error
  tput setaf 1 ; echo "[LOG] ${1}"
  tput sgr0    ; exit 1
}

func_successMsgWithLog () {
  tput setaf 2 ; echo "[LOG] ${1}"
  tput sgr0    ; exit 0
}

#   
func_splitNote () {
#   output the var "${input}" as absolut to the cli backlog? or what is it called?
#   pipe the output into a temporary file. Marked by the extention ".tmp"
#   put the input into a tmp file and encrypt it
  echo "${input}" | cat - "${input}" > cleartext.tmp
  func_encryptNote cleartext.tmp ciphertext.tmp "${pass}"

#   plain hexdump the ciphertext and store it, 
#   remove the plain text tmp files and move the encrypted tmp file into 
#   the user choosen name as directory.
  xxd -p ciphertext.tmp > cipherhex.tmp
  rm cleartext.tmp ciphertext.tmp ; mv cipherhex.tmp "${name}"

#   http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html
#   check if the string is non-zero
#   ToDo: check if " ! -z " can be replaced with " -n "; -n checks for non-zero string
  if [[ ! -z ${pieces} && -z ${bytes} ]] ; then

#   split into equal pieces
#   split {options} {file_name} {prefix}
#    split -l $(($(wc -l < ${name}) / ${pieces} + 1)) ${name} ${name}. --verbose
    split -l $(($(wc -l < ${name}) / ${pieces} + 1)) ${name} ${name}.
    #split -l $(($(wc -l < "${name}") / ((${pieces} + 1)))) "${name}" "${name}".
  elif [[ ! -z ${bytes} && -z ${pieces} ]] ; then

#   split into size specific pieces
    split -b "${bytes}" "${name}" "${name}".
  else
    rm "${name}" ; func_failMsgWithLog "Invalid number of arguments!"
  fi

  rm "${name}" ; func_checkDir "${name}"

#   for every piece in the dir, move a file/piece into the new dir
  for piece in "${name}".* ; do
      if [ "${piece}" == "${input}" ] ; then
        break
      fi

# HOWTO -> basename usage: "basename /home/user/data/myFilename11.txt .txt"
#       -> myFilename11
    mv "${piece}" "${name}"/"$(basename "${piece}")".leroy
  done

  func_successMsgWithLog "New NoteSpliter created in: '$(pwd)/${name}/'!"
}

#   joining the pieces
func_joinNotes () {

#   read sequentially all .leroy files in the user choosen directory
#   pipe it to hexdump (-r)evert (-p)lain
  cat ${input}/*.leroy | xxd -r -p > ciphertext.tmp

#   decrypt the hexdumped file with the user given param "${pass}"
  func_decryptNote ciphertext.tmp ${pass} > cleartext.tmp

#   get the original file name with "head", reading the fist line in the file
  ofilename=$(head -n 1 cleartext.tmp)

#   take the file content starting from the second line and move it into the output folder
  tail -n +2 cleartext.tmp > "${input}"/"${ofilename}"

  rm ciphertext.tmp cleartext.tmp
  func_successMsgWithLog "File rejoined as: '$(pwd)/${input}/${ofilename}'!"
}

#   use gpg to encrypt the entire file with a passphrase
#   this gpg call, could be a one liner but is split on to multiple for readability
# HOWTO   ${gpg} --symmetric --armor --batch --cipher-algo AES256 --passphrase-fd 3 --output "${2}" "${1}" 3< <(echo "${3}")
func_encryptNote () {
  ${gpg} \
    --symmetric --armor --batch \
    --cipher-algo AES256 --passphrase-fd 3 \
    --output "${2}" "${1}" 3< <(echo "${3}")
}

#   decrypt the input with gpg. 
#   this gpg call, could be a one liner but is split on to multiple for readability
func_decryptNote () {
  ${gpg} \
    --decrypt --armor --batch \
    --passphrase-fd 3 "${1}" 3< <(echo "${2}") 2>/dev/null
}

#   check if ! -exists OR if ! -exists AND is dir, if so, make a dir
func_checkDir () {
  if [[ ! -e ${1} || ! -d ${1} ]] ; then
    mkdir "${1}"
  fi
}

#   check if lenght is -zero AND not -xecutable, if so, print error msg
func_checkGPG () {
  if [[ -z ${gpg} && ! -x ${gpg} ]] ; then
    func_failMsgWithLog "GnuPG potion is not available"
    func_failMsgWithLog "GnuPG var: ${gpg}"
  fi
}

#                                                                                                     #
#                                                                                                     # 1 [START] 
#                                                                                                     # Below is the first call to a function in this script.
#                                                                                                     # This can be considered the start of the program flow.
#                                                                                                     #

#   init [start]
#   befor we do anything, let's check for gpg
func_checkGPG


#                                                                                                     #
#                                                                                                     # 1.1 
#                                                                                                     # The script starts with two ways branching. One for
#                                                                                                     # spliting the user choosen input into ".leroy" pieces, and
#                                                                                                     # one for joining ".leroy" pieces within a user defined directory.
#                                                                                                     #

#   check if the first parameter is equal to the absolut string 'split'
#   the gobal variable "${1}" is for storing the first parameter of the script call

#   this will kick off the creation process for a new NoteSpliter
#   If this is not the case, the script jumps to the next code block, such as "elif" or "else".
if [[ ${1} == 'split' ]] ; then

#   "shift" gets rid of the first cli parameter
#   create the var "args" and assign the parameters and set "--" to append $args
#   "getopt" takes parameter i and p assigns them after each other
  shift
  args=$(getopt i:n:p:s:b: $*)
  set -- $args
  for i ; do

# HOWTO -> syntax usage of bash "(swich) case in"
#
#   case  $variable-name  in
#     pattern1)       
#       command1
#       ;;
#     pattern2)
#       command1 ;;            
#     -p ) input="${2}" ; shift ;;
#     *) echo "[LOG testRun1 -case] Sorry, $variable-name could not be found";;
#     -- ) shift ; break ;;
#   esac

    case "$i" in
#   take the parameters and assign them to local vars
#   shift the current parameter away
      -i ) input="${2}"
           shift ; shift ;;
      -n ) name="${2}"
           shift ; shift ;;
      -p ) pass="${2}"
           shift ; shift ;;
      -s ) pieces="${2}"
           shift ; shift ;;
      -b ) bytes="${2}"
           shift ; shift ;;
      -- ) shift ; break ;;
    esac
  done


#                                                                                                     #
#                                                                                                     # 2 [START]
#                                                                                                     # Here we see the parameters get checked for and if
#                                                                                                     #

# HOWTO -> this first "if" block could be written like this:
#   if [[ -f ${input} && ! -z ${name} && ! -z ${pass} ]] ; then

#   check if file exists AND the var name and pass are non-zero
#   Here by are the "&&" operators used to check if the first and the n parameter are true.
  if [[ -f ${input} && ! -z ${name} && ! -z ${pass} ]] ; then

#   check if var pieces and bytes are non-zero
    if [[ ! -z ${pieces} || ! -z ${bytes} ]] ; then
      func_splitNote ${input} ${name} ${pass} ${pieces} ${bytes}
    else
      func_failMsgWithLog "[ERROR] Invalid number of arguments: check -s or -b"
    fi
  else
    echo "[DEBUG] args: ${input} ${name} ${pass} ${pieces} ${bytes}"
    func_failMsgWithLog "[ERROR] Input and/or name and/or passphrase are not valid! Please check your input."
  fi

#   check if the first parameter is equal to the absolut string 'join'
#   this will kick off a process to concatenat the given input files
elif [[ ${1} == 'join' ]] ; then
  shift

#   create the var "args" and assign the parameters and set "--" to append $args
#   "getopt" takes parameter i and p assigns them after each other
  args=$(getopt i:p: "$*")
  set -- $args
  for i ; do
    case "$i" in
      -i ) input="${2}"
           shift ; shift ;;
      -p ) pass="${2}"
           shift ; shift ;;
      -- ) shift ; break ;;
    esac
  done
#   check if input is a dir AND password is non-zero, if so, 
#   call func_joinNotes with input and password as parameters
  if [[ -d ${input} && ! -z ${pass} ]] ; then
    func_joinNotes "${input}" "${pass}"
  else
    func_failMsgWithLog "[ERROR] Invalid or missing parameter! Please provide all required parameters."
  fi
else
  func_failMsgWithLog "Unknown input: ${1}"
  echo "[NoteSpliter] USAGE: ./notespliter split -i file -n name -p pass -s n OR -b n[k|m]"
fi
#   init [end]

#                                                                                                     #
#                                                                                                     # 1, 2 [END] 
#                                                                             
